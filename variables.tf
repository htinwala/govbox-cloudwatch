#################### Common ######################
variable "default_region" {
  description = "AWS Region where components will be created/configured"
}

variable "default_tags" {}

#################### Common ######################

#################### SNS Topic ################

variable "sns_email_topic_name" {
  type        = string
  description = "The descriptive name for the sns topic."
}

variable "sns_email_display_name" {
  type        = string
  description = "Name shown in confirmation emails"
}

variable "sns_email_addresses" {
  type        = list
  description = "Email address to send notifications to"
}

#################### SNS Topic ################

#################### Cloud Watch ################

# EC2 CPU Utilization Alarm
variable "cpu_util_alarm_enabled" {
  type        = bool
  description = "Enable alarm."
}

variable "cpu_util_alarm_name" {
  type        = string
  description = "The descriptive name for the alarm."
}

variable "cpu_util_alarm_description" {
  type        = string
  description = "The description for the alarm."
}

variable "cpu_util_comparison_operator" {
  type        = string
  description = "The arithmetic operation to use when comparing the specified Statistic and Threshold."
}

variable "cpu_util_evaluation_periods" {
  type        = number
  description = "The number of periods over which data is compared to the specified threshold."
}

variable "cpu_util_metric_name" {
  type        = string
  description = "The name for the alarm's associated metric."
}

variable "cpu_util_namespace" {
  type        = string
  description = "The namespace for the alarm's associated metric."
}

variable "cpu_util_period" {
  type        = number
  description = "The period in seconds over which the specified statistic is applied."
}

variable "cpu_util_statistic" {
  type        = string
  description = "The statistic to apply to the alarm's associated metric."
}

variable "cpu_util_threshold" {
  type        = number
  description = "The value against which the specified statistic is compared."
}

variable "cpu_util_actions_enabled" {
  type        = bool
  description = "Indicates whether or not actions should be executed during any changes to the alarm's state."
}

# variable "cpu_util_alarm_actions" {
#   type        = list
#   description = "The list of actions to execute when this alarm transitions into an ALARM state from any other state."
# }

variable "cpu_util_insufficient_data_actions" {
  type        = list
  description = "The list of actions to execute when this alarm transitions into an INSUFFICIENT_DATA state from any other state."
}

variable "cpu_util_ok_actions" {
  type        = list
  description = "The list of actions to execute when this alarm transitions into an OK state from any other state."
}

# variable "cpu_util_dimensions" {
# }
# EC2 CPU Utilization Alarm

# EC2 Status Check Alarm
variable "ec2_stts_chck_alarm_enabled" {
  type        = bool
  description = "Enable alarm."
}

variable "ec2_stts_chck_alarm_name" {
  type        = string
  description = "The descriptive name for the alarm."
}

variable "ec2_stts_chck_alarm_description" {
  type        = string
  description = "The description for the alarm."
}

variable "ec2_stts_chck_comparison_operator" {
  type        = string
  description = "The arithmetic operation to use when comparing the specified Statistic and Threshold."
}

variable "ec2_stts_chck_evaluation_periods" {
  type        = number
  description = "The number of periods over which data is compared to the specified threshold."
}

variable "ec2_stts_chck_metric_name" {
  type        = string
  description = "The name for the alarm's associated metric."
}

variable "ec2_stts_chck_namespace" {
  type        = string
  description = "The namespace for the alarm's associated metric."
}

variable "ec2_stts_chck_period" {
  type        = number
  description = "The period in seconds over which the specified statistic is applied."
}

variable "ec2_stts_chck_statistic" {
  type        = string
  description = "The statistic to apply to the alarm's associated metric."
}

variable "ec2_stts_chck_threshold" {
  type        = number
  description = "The value against which the specified statistic is compared."
}

variable "ec2_stts_chck_actions_enabled" {
  type        = bool
  description = "Indicates whether or not actions should be executed during any changes to the alarm's state."
}

# variable "ec2_stts_chck_alarm_actions" {
#   type        = list
#   description = "The list of actions to execute when this alarm transitions into an ALARM state from any other state."
# }

variable "ec2_stts_chck_insufficient_data_actions" {
  type        = list
  description = "The list of actions to execute when this alarm transitions into an INSUFFICIENT_DATA state from any other state."
}

variable "ec2_stts_chck_ok_actions" {
  type        = list
  description = "The list of actions to execute when this alarm transitions into an OK state from any other state."
}

# variable "ec2_stts_chck_dimensions" {
# }
# EC2 Status Check Alarm

# RDS CPU Utilization Alarm
variable "rds_cpu_util_alarm_enabled" {
  type        = bool
  description = "Enable alarm."
}

variable "rds_cpu_util_alarm_name" {
  type        = string
  description = "The descriptive name for the alarm."
}

variable "rds_cpu_util_alarm_description" {
  type        = string
  description = "The description for the alarm."
}

variable "rds_cpu_util_comparison_operator" {
  type        = string
  description = "The arithmetic operation to use when comparing the specified Statistic and Threshold."
}

variable "rds_cpu_util_evaluation_periods" {
  type        = number
  description = "The number of periods over which data is compared to the specified threshold."
}

variable "rds_cpu_util_metric_name" {
  type        = string
  description = "The name for the alarm's associated metric."
}

variable "rds_cpu_util_namespace" {
  type        = string
  description = "The namespace for the alarm's associated metric."
}

variable "rds_cpu_util_period" {
  type        = number
  description = "The period in seconds over which the specified statistic is applied."
}

variable "rds_cpu_util_statistic" {
  type        = string
  description = "The statistic to apply to the alarm's associated metric."
}

variable "rds_cpu_util_threshold" {
  type        = number
  description = "The value against which the specified statistic is compared."
}

variable "rds_cpu_util_actions_enabled" {
  type        = bool
  description = "Indicates whether or not actions should be executed during any changes to the alarm's state."
}

# variable "rds_cpu_util_alarm_actions" {
#   type        = list
#   description = "The list of actions to execute when this alarm transitions into an ALARM state from any other state."
# }

variable "rds_cpu_util_insufficient_data_actions" {
  type        = list
  description = "The list of actions to execute when this alarm transitions into an INSUFFICIENT_DATA state from any other state."
}

variable "rds_cpu_util_ok_actions" {
  type        = list
  description = "The list of actions to execute when this alarm transitions into an OK state from any other state."
}

# variable "rds_cpu_util_dimensions" {
# }
# RDS CPU Utilization Alarm

# RDS Storage Availability Alarm
variable "rds_storage_alarm_enabled" {
  type        = bool
  description = "Enable alarm."
}

variable "rds_storage_alarm_name" {
  type        = string
  description = "The descriptive name for the alarm."
}

variable "rds_storage_alarm_description" {
  type        = string
  description = "The description for the alarm."
}

variable "rds_storage_comparison_operator" {
  type        = string
  description = "The arithmetic operation to use when comparing the specified Statistic and Threshold."
}

variable "rds_storage_evaluation_periods" {
  type        = number
  description = "The number of periods over which data is compared to the specified threshold."
}

variable "rds_storage_metric_name" {
  type        = string
  description = "The name for the alarm's associated metric."
}

variable "rds_storage_namespace" {
  type        = string
  description = "The namespace for the alarm's associated metric."
}

variable "rds_storage_period" {
  type        = number
  description = "The period in seconds over which the specified statistic is applied."
}

variable "rds_storage_statistic" {
  type        = string
  description = "The statistic to apply to the alarm's associated metric."
}

variable "rds_storage_threshold" {
  type        = number
  description = "The value against which the specified statistic is compared."
}

variable "rds_storage_actions_enabled" {
  type        = bool
  description = "Indicates whether or not actions should be executed during any changes to the alarm's state."
}

# variable "rds_storage_alarm_actions" {
#   type        = list
#   description = "The list of actions to execute when this alarm transitions into an ALARM state from any other state."
# }

variable "rds_storage_insufficient_data_actions" {
  type        = list
  description = "The list of actions to execute when this alarm transitions into an INSUFFICIENT_DATA state from any other state."
}

variable "rds_storage_ok_actions" {
  type        = list
  description = "The list of actions to execute when this alarm transitions into an OK state from any other state."
}

# variable "rds_storage_dimensions" {
# }
# RDS Storage Availability Alarm

#################### Cloud Watch ################
