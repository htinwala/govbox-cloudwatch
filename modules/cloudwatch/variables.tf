variable "enabled" {
  type        = bool
  description = "Enable alarm."
}

variable "alarm_name" {
  type        = string
  description = "The descriptive name for the alarm."
}

variable "alarm_description" {
  type        = string
  description = "The description for the alarm."
}

variable "comparison_operator" {
  type        = string
  description = "The arithmetic operation to use when comparing the specified Statistic and Threshold."
}

variable "evaluation_periods" {
  type        = number
  description = "The number of periods over which data is compared to the specified threshold."
}

variable "metric_name" {
  type        = string
  description = "The name for the alarm's associated metric."
}

variable "namespace" {
  type        = string
  description = "The namespace for the alarm's associated metric."
}

variable "period" {
  type        = number
  description = "The period in seconds over which the specified statistic is applied."
}

variable "statistic" {
  type        = string
  description = "The statistic to apply to the alarm's associated metric."
}

variable "threshold" {
  type        = number
  description = "The value against which the specified statistic is compared."
}

variable "actions_enabled" {
  type        = bool
  description = "Indicates whether or not actions should be executed during any changes to the alarm's state."
}

variable "alarm_actions" {
  type        = list
  description = "The list of actions to execute when this alarm transitions into an ALARM state from any other state."
}

variable "insufficient_data_actions" {
  type        = list
  description = "The list of actions to execute when this alarm transitions into an INSUFFICIENT_DATA state from any other state."
}

variable "ok_actions" {
  type        = list
  description = "The list of actions to execute when this alarm transitions into an OK state from any other state."
}

variable "tags" {
}
