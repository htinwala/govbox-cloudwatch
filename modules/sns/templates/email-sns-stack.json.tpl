{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Resources" : {
      "EmailSNSTopic": {
        "Type" : "AWS::SNS::Topic",
        "Properties" : {
          "TopicName": "${sns_email_topic_name}",
          "DisplayName" : "${sns_display_name}",
          "Subscription": [
            ${sns_subscription_list}
          ]
        }
      }
    },
    "Outputs" : {
      "ARN" : {
        "Description" : "Email SNS Topic ARN",
        "Value" : { "Ref" : "EmailSNSTopic" }
      }
    }
}