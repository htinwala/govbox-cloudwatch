variable "sns_email_topic_name" {
  type        = string
  description = "The descriptive name for the sns topic."
}

variable "tags" {
}

variable "sns_email_topic_display_name" {
  type        = string
  description = "Name shown in confirmation emails"
}

variable "sns_subscription_email_address_list" {
  type        = list
  description = "Email address to send notifications to"
}

variable "sns_subscription_protocol" {
  type        = string
  default     = "email"
  description = "SNS Protocol to use. email or email-json"
}

variable "sns_email_subscription_stack_name" {
  type        = string
  description = "Unique Cloudformation stack name that wraps the SNS topic."
}