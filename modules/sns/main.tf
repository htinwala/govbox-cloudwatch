# Create an SNS topic for the Cloudwatch alerts

# resource "aws_sns_topic" "cloud_watch_alerts_topic" {
#   name = var.sns_email_topic_name
#   tags = var.tags
# }

data "template_file" "cloudformation_sns_stack" {
  template = file("${path.module}/templates/email-sns-stack.json.tpl")
  vars     = {
    sns_email_topic_name  = var.sns_email_topic_name
    sns_display_name      = var.sns_email_topic_display_name
    sns_subscription_list = join(",", formatlist("{ \"Endpoint\": \"%s\", \"Protocol\": \"%s\"  }", var.sns_subscription_email_address_list, var.sns_subscription_protocol))
  }
}

resource "aws_cloudformation_stack" "sns_topic_cf_stack" {
  name          = var.sns_email_subscription_stack_name
  template_body = data.template_file.cloudformation_sns_stack.rendered
  tags          = merge(var.tags, map("Name", var.sns_email_subscription_stack_name))
}