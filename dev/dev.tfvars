#--------------------------------------------------------------
#                     General
#--------------------------------------------------------------
 # Mandatory - Update default_tags items before deployment
 default_tags = {
  Owner = "huzefa.tinwala@slalom.com"
  Team = "RDT-WEST"
  Environment = "Dev"
  Application = "GOVbox-CloudWatch"
 }

  default_region = "us-east-2"

#--------------------------------------------------------------
#                     SNS TOPIC
#--------------------------------------------------------------

sns_email_topic_name = "cloud-watch-email-alerts"
sns_email_display_name = "Cloudwatch Alerts"
sns_email_addresses = ["huzefa.tinwala@slalom.com"] # ["user1@example.com", "user2@example.com"]

#--------------------------------------------------------------
#                     SNS TOPIC
#--------------------------------------------------------------

#--------------------------------------------------------------
#                     CLOUD WATCH ALARMS
#--------------------------------------------------------------

##################### EC2 CPU UTILIZATION ALARM #####################
  cpu_util_alarm_enabled             = true
  cpu_util_alarm_name                = "ec2-cpu-util-threshold"
  cpu_util_alarm_description         = "This metric monitors EC2 CPU utilization"
  cpu_util_comparison_operator       = "GreaterThanThreshold"
  cpu_util_evaluation_periods        = "2"
  cpu_util_metric_name               = "CPUUtilization"
  cpu_util_namespace                 = "AWS/EC2"
  cpu_util_period                    = "600" # 10 minutes - Period value in seconds over which the statistic is applied
  cpu_util_statistic                 = "Average"
  cpu_util_threshold                 = "80" # Maximum % of CPU utilization permissible
  cpu_util_actions_enabled           = true
  # cpu_util_alarm_actions             = []
  cpu_util_insufficient_data_actions = []
  cpu_util_ok_actions                = []
  # cpu_util_dimensions { }
##################### EC2 CPU UTILIZATION ALARM #####################

##################### EC2 STATUS CHECK ALARM ########################
  ec2_stts_chck_alarm_enabled             = true
  ec2_stts_chck_alarm_name                = "ec2-status-check"
  ec2_stts_chck_alarm_description         = "This metric monitors EC2 Status Check Failures"
  ec2_stts_chck_comparison_operator       = "GreaterThanOrEqualToThreshold"
  ec2_stts_chck_evaluation_periods        = "2"
  ec2_stts_chck_metric_name               = "StatusCheckFailed"
  ec2_stts_chck_namespace                 = "AWS/EC2"
  ec2_stts_chck_period                    = "300" # 5 minutes - Period value in seconds over which the statistic is applied
  ec2_stts_chck_statistic                 = "Maximum"
  ec2_stts_chck_threshold                 = "1.0" # Minimum number of instance failures on which to trigger the alarm
  ec2_stts_chck_actions_enabled           = true
  # ec2_stts_chck_alarm_actions             = []
  ec2_stts_chck_insufficient_data_actions = []
  ec2_stts_chck_ok_actions                = []
  # ec2_stts_chck_dimensions { }
##################### EC2 STATUS CHECK ALARM ########################

##################### RDS CPU UTILIZATION ALARM #####################
  rds_cpu_util_alarm_enabled             = true
  rds_cpu_util_alarm_name                = "rds-cpu-util-threshold"
  rds_cpu_util_alarm_description         = "This metric monitors RDS CPU utilization"
  rds_cpu_util_comparison_operator       = "GreaterThanThreshold"
  rds_cpu_util_evaluation_periods        = "1"
  rds_cpu_util_metric_name               = "CPUUtilization"
  rds_cpu_util_namespace                 = "AWS/RDS"
  rds_cpu_util_period                    = "600" # 10 minutes - Period value in seconds over which the statistic is applied
  rds_cpu_util_statistic                 = "Average"
  rds_cpu_util_threshold                 = "80" # Maximum % of CPU utilization permissible
  rds_cpu_util_actions_enabled           = true
  # rds_cpu_util_alarm_actions             = []
  rds_cpu_util_insufficient_data_actions = []
  rds_cpu_util_ok_actions                = []
  # rds_cpu_util_dimensions { }
##################### RDS CPU UTILIZATION ALARM #####################

##################### RDS STORAGE SPACE ALARM #####################
  rds_storage_alarm_enabled             = true
  rds_storage_alarm_name                = "free_storage_space_threshold"
  rds_storage_alarm_description         = "This metric monitors RDS free storage space availability"
  rds_storage_comparison_operator       = "LessThanThreshold"
  rds_storage_evaluation_periods        = "1"
  rds_storage_metric_name               = "FreeStorageSpace"
  rds_storage_namespace                 = "AWS/RDS"
  rds_storage_period                    = "600" # 10 minutes - Period value in seconds over which the statistic is applied
  rds_storage_statistic                 = "Average"
  rds_storage_threshold                 = "2000000000" # 2GB - Minimum amount of available storage space permissible
  rds_storage_actions_enabled           = true
  # rds_storage_alarm_actions             = []
  rds_storage_insufficient_data_actions = []
  rds_storage_ok_actions                = []
  # rds_storage_dimensions { }
##################### RDS STORAGE SPACE ALARM #####################

#--------------------------------------------------------------
#                     CLOUD WATCH ALARMS
#--------------------------------------------------------------
