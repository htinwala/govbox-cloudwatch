provider "aws" {
  region = var.default_region
}

# Module - SNS Topic for Cloudwatch alarms
module "sns" {
  source                               = "./modules/sns"
  tags                                 = var.default_tags
  sns_email_topic_name                 = var.sns_email_topic_name
  sns_email_topic_display_name         = var.sns_email_display_name
  sns_subscription_email_address_list  = var.sns_email_addresses
  sns_subscription_protocol            = "email"
  sns_email_subscription_stack_name    = "sns-email-cf-stack"
}

# Module - Cloud Watch Alarm for EC2 CPU Utilization
module "cpu_util_alarm" {
  source                    = "./modules/cloudwatch"
  tags                      = var.default_tags
  enabled                   = var.cpu_util_alarm_enabled
  alarm_name                = var.cpu_util_alarm_name
  alarm_description         = var.cpu_util_alarm_description
  comparison_operator       = var.cpu_util_comparison_operator
  evaluation_periods        = var.cpu_util_evaluation_periods
  metric_name               = var.cpu_util_metric_name
  namespace                 = var.cpu_util_namespace
  period                    = var.cpu_util_period
  statistic                 = var.cpu_util_statistic
  threshold                 = var.cpu_util_threshold
  actions_enabled           = var.cpu_util_actions_enabled
  alarm_actions             = [module.sns.sns_topic_arn]
  insufficient_data_actions = var.cpu_util_insufficient_data_actions
  ok_actions                = var.cpu_util_ok_actions
}

# Module - Cloud Watch Alarm for EC2 Status Check Failures
module "ec2_stts_chck_alarm" {
  source                    = "./modules/cloudwatch"
  tags                      = var.default_tags
  enabled                   = var.ec2_stts_chck_alarm_enabled
  alarm_name                = var.ec2_stts_chck_alarm_name
  alarm_description         = var.ec2_stts_chck_alarm_description
  comparison_operator       = var.ec2_stts_chck_comparison_operator
  evaluation_periods        = var.ec2_stts_chck_evaluation_periods
  metric_name               = var.ec2_stts_chck_metric_name
  namespace                 = var.ec2_stts_chck_namespace
  period                    = var.ec2_stts_chck_period
  statistic                 = var.ec2_stts_chck_statistic
  threshold                 = var.ec2_stts_chck_threshold
  actions_enabled           = var.ec2_stts_chck_actions_enabled
  alarm_actions             = [module.sns.sns_topic_arn]
  insufficient_data_actions = var.ec2_stts_chck_insufficient_data_actions
  ok_actions                = var.ec2_stts_chck_ok_actions
}

# Module - Cloud Watch Alarm for RDS CPU Utilization
module "rds_cpu_util_alarm" {
  source                    = "./modules/cloudwatch"
  tags                      = var.default_tags
  enabled                   = var.rds_cpu_util_alarm_enabled
  alarm_name                = var.rds_cpu_util_alarm_name
  alarm_description         = var.rds_cpu_util_alarm_description
  comparison_operator       = var.rds_cpu_util_comparison_operator
  evaluation_periods        = var.rds_cpu_util_evaluation_periods
  metric_name               = var.rds_cpu_util_metric_name
  namespace                 = var.rds_cpu_util_namespace
  period                    = var.rds_cpu_util_period
  statistic                 = var.rds_cpu_util_statistic
  threshold                 = var.rds_cpu_util_threshold
  actions_enabled           = var.rds_cpu_util_actions_enabled
  alarm_actions             = [module.sns.sns_topic_arn]
  insufficient_data_actions = var.rds_cpu_util_insufficient_data_actions
  ok_actions                = var.rds_cpu_util_ok_actions
}

# Module - Cloud Watch Alarm for RDS Storage Space Availability
module "rds_storage_alarm" {
  source                    = "./modules/cloudwatch"
  tags                      = var.default_tags
  enabled                   = var.rds_storage_alarm_enabled
  alarm_name                = var.rds_storage_alarm_name
  alarm_description         = var.rds_storage_alarm_description
  comparison_operator       = var.rds_storage_comparison_operator
  evaluation_periods        = var.rds_storage_evaluation_periods
  metric_name               = var.rds_storage_metric_name
  namespace                 = var.rds_storage_namespace
  period                    = var.rds_storage_period
  statistic                 = var.rds_storage_statistic
  threshold                 = var.rds_storage_threshold
  actions_enabled           = var.rds_storage_actions_enabled
  alarm_actions             = [module.sns.sns_topic_arn]
  insufficient_data_actions = var.rds_storage_insufficient_data_actions
  ok_actions                = var.rds_storage_ok_actions
}
